<div class="well clearfix">
  <form class="form-horizontal acerto" role="form" method="post" action="<?php echo base_url('atendimento/consulta') ?>" onsubmit="overlay(true)">
    <fieldset class="col-sm-3">
      <div class="form-group">
        <label for="tipo">Tipo</label>
        <select class="form-control" id="tipo" name="tipo" autofocus>
          <option value="aberto">Abertos</option>
          <option value="fechado">Fechados</option>
        </select>
      </div>
      <div class="form-group">
        <label for="fila">Fila Nível 1</label>
        <select class="form-control" id="fila_atendimento" name="fila">
          <option value="ALL">Todos</option>
          <?php foreach ($fila as $row) : ?>
            <option value="<?php echo $row; ?>"><?php echo $row; ?></option>
          <?php endforeach; ?>
        </select>
      </div>
    </fieldset>
    <fieldset class="col-sm-4 col-sm-offset-1 grupo2 none">

      <div class="form-group pergunta">
        <label for="datade">Data de</label>
        <input type="month" class="form-control" id="datade" name="datade" placeholder="Data de" />
      </div>
      <div class="form-group pergunta">
        <label for="dataate">Data até</label>
        <input type="month" class="form-control" id="dataate" name="dataate" placeholder="Data até" />
      </div>

    </fieldset>
    <fieldset class="col-sm-3 col-sm-offset-1">

      <div class="form-group">
        <label for="responsavel">Responsável</label>
        <select class="form-control" id="responsavel" name="responsavel">
          <option value="ALL">Todos</option>
          <?php foreach ($responsavel as $row) : ?>
            <option value="<?php echo $row['Responsavel']; ?>">
              <?php echo $row['Responsavel']; ?>
            </option>
          <?php endforeach; ?>
        </select>
      </div>

      <div class="form-group pergunta">
        <label for="proprietario">Proprietário</label>
        <select class="form-control chosen" multiple="true" id="proprietario" name="proprietario[]">
          <?php foreach ($proprietario as $row) : ?>
            <option value="<?php echo $row['Proprietario']; ?>"><?php echo $row['Proprietario']; ?></option>
          <?php endforeach; ?>
        </select>
      </div>

      <div class="form-group">
        <label for="status">Status</label>
        <select class="form-control" id="status" name="status">
          <option value="ALL">Todos</option>
          <?php foreach ($status as $row) : ?>
            <option value="<?php echo $row['Status']; ?>"><?php echo $row['Status']; ?></option>
          <?php endforeach; ?>
        </select>
      </div>
    </fieldset>
    <fieldset class="col-sm-1 col-sm-offset-11">
      <div class="form-group pull-right">
        <button type="submit" class="m-btn black">Buscar</button>
      </div>
    </fieldset>
  </form>
</div>
<?php if (isset($resultado)): ?>
  <fieldset class="col-sm-12">
    <p class="text-muted">Registros de busca por: <?php echo $resultado == '' ? 'ALL' : $resultado; ?></p>
  </fieldset>
<?php endif; ?>
<?php if (isset($lista)): ?>
  <div class="table-responsive">
    <table class="table table-striped table-bordered table-hover table-condensed datatable">
      <thead>
        <tr>
          <th>CHAMADO</th>
          <th>ASSUNTO</th>
          <th>FILA</th>
          <th>DATA ABERTURA</th>
          <th>STATUS</th>
          <th>TOTVS</th>
          <th>CLIENTE</th>
          <th>DEPTO. CLIENTE</th>
          <th>PROPRIETÁRIO</th>
        </tr>
      </thead>
      <tbody>
        <?php foreach ($lista as $row): ?>
          <tr>
            <td><a href="http://otrs.grupompe.com.br/otrs/index.pl?Action=AgentTicketZoom;TicketID=<?php echo $row[model_listagem_geral::ID]; ?>"  target="_new"><?php echo $row[model_listagem_geral::CHAMADO]; ?></a></td>
            <td><?php echo $row[model_listagem_geral::ASSUNTO]; ?></td>
            <td><span title="Serviço <?php echo $row[model_listagem_geral::SERVICO]; ?>" class="tooltips"><?php echo $row[model_listagem_geral::FILA]; ?></span></td>
            <td><span title="<?php echo $row[model_listagem_geral::DIAS_ABERTO]; ?> dia(s) em aberto" class="tooltips"><?php echo date_format(date_create($row[model_listagem_geral::ABERTURA]), 'd/m/Y'); ?></span></td>
            <td><?php echo $row[model_listagem_geral::STATUS]; ?></td>
            <td><?php echo $row[model_listagem_geral::TOTVS]; ?></td>
            <td><?php echo explode('@', $row[model_listagem_geral::CLIENTE])[0]; ?></td>
            <td><?php echo $row[model_listagem_geral::DEPTO]; ?></td>
            <td><span class="tooltips" title="Responsável: <?php echo $row[model_listagem_geral::RESPONSAVEL]; ?>"><?php echo $row[model_listagem_geral::PROPRIETARIO]; ?></span></td>
          </tr>
        <?php endforeach; ?>
      </tbody>
      <tfoot>
        <tr>
          <td colspan="9">
            <form class="form-horizontal" role="form" method="post" action="<?php echo base_url('atendimento/consulta/exportaExcel') ?>" target="_blank">
              <input type="hidden" name="dados" value='<?php echo $excel; ?>' />
              <button type="submit" class="m-btn green tooltips pull-right" title="Exportar">
            <i class="fa fa-download"></i> Excel
          </button>
            </form>
          </td>
        </tr>
      </tfoot>
    </table>
  </div>
<?php endif; ?>